#Import Libraries
import cv2 as cv
import sys

#Define webcam for video input
vid = cv.VideoCapture(0, cv.CAP_DSHOW)

#Pointer to HAAR cascade file
faceCascade = cv.CascadeClassifier(cv.data.haarcascades + "haarcascade_frontalface_default.xml")
eyeCascade = cv.CascadeClassifier(cv.data.haarcascades + "haarcascade_profileface.xml ")

while True:
    #Create windows that show boxes
    ret, frame = vid.read()
    ret, frame2 = vid.read()

    #Grayscale frames for easier recognition
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    gray2 = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)

    #Set boundaries
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(90, 90))

    #Make rectangles around detected faces
    for (x, y, w, h) in faces:
        cv.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    eyes = eyeCascade.detectMultiScale(
        gray2,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(90, 90))

    for (x, y, w, h) in eyes:
        cv.rectangle(frame2, (x, y), (x + w, y + h), (0, 255, 0), 2)

    #Make another window and show faces with rectangles applied
    cv.imshow("Faces found", frame)
    print("Found {0} faces!".format(len(faces)))

    cv.imshow("Eyeglasses found", frame2)
    print("Found {0} eyes!".format(len(eyes)))

    #Hit 'q' to end program DONT WORK YET
    if cv.waitKey(1) & 0xFF == ord('q'):
        break

vid.release()
cv.destroyAllWindows()
sys.exit()
